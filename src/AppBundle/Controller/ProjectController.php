<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\project;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
class ProjectController extends Controller
{
    /**
     * @Route("/", name="project_list")
     */
      public function listAction(Request $request)
    {

        $reports=$this -> getDoctrine() -> getRepository('AppBundle:project')->findAll();
        return $this->render('project/list.html.twig',array('reports'=>$reports));
    }

        /**
     * @Route("/rejected", name="project_rejected")
     */
      public function rejectedAction(Request $request)
    {

        $reports=$this -> getDoctrine() -> getRepository('AppBundle:project')->findAll();
        return $this->render('project/rejected.html.twig',array('reports'=>$reports));
    }

    /**
     * @Route("/add", name="project_add")
     */
    public function addAction(Request $request)
    {
        $report = new project;
        $form = $this->createFormBuilder($report) 
        -> add('name', TextType::class, array('attr'=> array('class'=>'form-control', 'style'=>'margin-bottom:15px' )))
        -> add('priority', ChoiceType::class, array('choices' => array('Low'=>'low', 'High'=> 'high'), 'attr'=> array('class'=>'form-control', 'style'=>'margin-bottom:15px' )))
        -> add('description', TextareaType::class, array('attr'=> array('class'=>'form-control', 'style'=>'margin-bottom:15px' )))
        -> add('type', ChoiceType::class, array('choices' => array('General' => 'general', 'Site' => 'site'),'label' => 'Type (if General enter cause, if Site enter coordinates)', 'attr'=> array('class'=>'form-control', 'style'=>'margin-bottom:15px' )))
        -> add('cause', TextType::class, array('required'=>false, 'attr'=> array('class'=>'form-control', 'style'=>'margin-bottom:15px')))
        -> add('longitude', TextType::class, array('required'=>false,'attr'=> array('class'=>'form-control', 'style'=>'margin-bottom:15px' )))
        -> add('latitude', TextType::class, array('required'=>false,'attr'=> array('class'=>'form-control', 'style'=>'margin-bottom:15px' )))
        -> add('Submit', SubmitType::class, array('label' => 'Add Report','attr'=> array('class'=>'btn btn-success', 'style'=>'margin-bottom:15px' )))

        -> getForm();

        $form->handleRequest($request);
        if ($form ->isSubmitted() && $form->isValid())
        {
            $name=$form['name']->getData();
            $priority=$form['priority']->getData();
            $description=$form['description']->getData();
            $type=$form['type']->getData();
            if ($type=="general")
            $cause=$form['cause']->getData();
            else
            {
                $longitude=$form['longitude']->getData();
                $latitude=$form['latitude']->getData();
             }

            $report->setName($name);
            $report->setPriority($priority);
            $report->setDescription($description);
            $report->setType($type);
            if ($type=="general")
            {
                $report->setCause($cause);
                $report->setLongitude(0);
                $report->setLatitude(0);
            }
            else 
            {
                $report->setCause('None');
                $report->setLongitude($longitude);
                $report->setLatitude($latitude);
            }

            $report->setApproved(0);
            $report->setAccepted(0);
            $report->setJustification(' ');
            $em = $this->getDoctrine()->getManager();
            $em->persist($report);
            $em->flush();

            $this->addFlash('notice', 'Reported Added');
            return $this->redirectToRoute('project_list');



        }

        return $this->render('project/add.html.twig', array('form'=>$form->createView()));
    }

    

     /**
     * @Route("/edit/{id}", name="project_edit")
     */
      public function editAction($id, Request $request)
    {

        $report=$this -> getDoctrine() -> getRepository('AppBundle:project')->find($id);
        $report2=clone $report;



        $form = $this->createFormBuilder($report) 
        -> add('name', TextType::class, array('attr'=> array('class'=>'form-control', 'style'=>'margin-bottom:15px' )))
        -> add('priority', ChoiceType::class, array('choices' => array('Low'=>'low', 'High'=> 'high'), 'attr'=> array('class'=>'form-control', 'style'=>'margin-bottom:15px' )))
        -> add('description', TextareaType::class, array('attr'=> array('class'=>'form-control', 'style'=>'margin-bottom:15px' )))
        -> add('type', ChoiceType::class, array('choices' => array('General' => 'general', 'Site' => 'site'),'label' => 'Type (if General enter cause, if Site enter coordinates)', 'attr'=> array('class'=>'form-control', 'style'=>'margin-bottom:15px' )))
        -> add('cause', TextType::class, array('required'=>false, 'attr'=> array('class'=>'form-control', 'style'=>'margin-bottom:15px')))
        -> add('longitude', TextType::class, array('required'=>false,'attr'=> array('class'=>'form-control', 'style'=>'margin-bottom:15px' )))
        -> add('latitude', TextType::class, array('required'=>false,'attr'=> array('class'=>'form-control', 'style'=>'margin-bottom:15px' )))
        -> add('Submit', SubmitType::class, array('label' => 'Update Report','attr'=> array('class'=>'btn btn-success', 'style'=>'margin-bottom:15px' )))

        -> getForm();

        $form->handleRequest($request);
        if ($form ->isSubmitted() && $form->isValid())
        {
            $name=$form['name']->getData();
            $priority=$form['priority']->getData();
            $description=$form['description']->getData();
            $type=$form['type']->getData();
            if ($type=="general")
            $cause=$form['cause']->getData();
            else
            {
                $longitude=$form['longitude']->getData();
                $latitude=$form['latitude']->getData();
             }

            $em = $this->getDoctrine()->getManager();
            $report=$em->getRepository('AppBundle:project')->find ($id);


            $report->setName($name);
            $report->setPriority($priority);
            $report->setDescription($description);
            $report->setType($type);
            if ($type=="general")
            {
                $report->setCause($cause);
                $report->setLongitude(0);
                $report->setLatitude(0);
            }
            else 
            {
                $report->setCause('None');
                $report->setLongitude($longitude);
                $report->setLatitude($latitude);
            }

            $report->setApproved(0);
            $report->setAccepted(0);
            $report->setJustification(' ');
            


            $em->persist($report2);
            $em->flush();

            $this->addFlash('notice', 'Report Updated');
            return $this->redirectToRoute('project_list');



        }




        return $this->render('project/edit.html.twig',array('report'=>$report, 'form'=>$form->createView()));
    }

     /**
     * @Route("/approve", name="project_approve")
     */
      public function approveAction(Request $request)
    {
        $reports=$this -> getDoctrine() -> getRepository('AppBundle:project')->findAll();

        return $this->render('project/approve.html.twig',array('reports'=>$reports));
    
    }
     /**
     * @Route("/view/{id}", name="project_view")
     */
      public function viewAction($id )
    {

        $report=$this -> getDoctrine() -> getRepository('AppBundle:project')->find($id);
        return $this->render('project/view.html.twig',array('report'=>$report));
    }

    /**
     * @Route("/approval/{id}", name="project_approval")
     */
    
        public function approvalAction($id)
        {
            $em = $this->getDoctrine()->getManager();
            $report = $em ->getRepository('AppBundle:project')->find($id);
            $old = $em ->getRepository('AppBundle:project')->findByName($report->getName());
            foreach ($old as $t)
                $em->remove($t);
            $report->setApproved(1);
            $report->setAccepted(1);
            $em->persist($report);
            $em->flush();

            $this->addFlash('notice', 'Reported Approved');
            return $this->redirectToRoute('project_list');
        }

          /**
     * @Route("/reject/{id}", name="project_reject")
     */
    
        public function rejectAction($id, Request $request)
        {
            $em = $this->getDoctrine()->getManager();
            $report = $em ->getRepository('AppBundle:project')->find($id);
            $form = $this->createFormBuilder($report) 
         -> add('name', TextType::class, array('attr'=> array('class'=>'form-control', 'style'=>'margin-bottom:15px' )))
         -> add('priority', ChoiceType::class, array('choices' => array('Low'=>'low', 'High'=> 'high'), 'attr'=> array('class'=>'form-control', 'style'=>'margin-bottom:15px' )))
         -> add('description', TextareaType::class, array('attr'=> array('class'=>'form-control', 'style'=>'margin-bottom:15px' )))
         -> add('type', ChoiceType::class, array('choices' => array('General' => 'general', 'Site' => 'site'),'label' => 'Type (if General enter cause, if Site enter coordinates)', 'attr'=> array('class'=>'form-control', 'style'=>'margin-bottom:15px' )))
         -> add('cause', TextType::class, array('required'=>false, 'attr'=> array('class'=>'form-control', 'style'=>'margin-bottom:15px')))
         -> add('longitude', TextType::class, array('required'=>false,'attr'=> array('class'=>'form-control', 'style'=>'margin-bottom:15px' )))
         -> add('latitude', TextType::class, array('required'=>false,'attr'=> array('class'=>'form-control', 'style'=>'margin-bottom:15px' )))
         -> add('justification', TextType::class, array('required'=>false,'attr'=> array('class'=>'form-control', 'style'=>'margin-bottom:15px' )))
         -> add('Submit', SubmitType::class, array('label' => 'Reject Report','attr'=> array('class'=>'btn btn-success', 'style'=>'margin-bottom:15px' )))
         -> getForm();
         $form->handleRequest($request);
        if ($form ->isSubmitted() && $form->isValid())
        {
            $name=$form['name']->getData();
            $priority=$form['priority']->getData();
            $description=$form['description']->getData();
            $type=$form['type']->getData();
            $justification=$form['justification']->getData();
            if ($type=="general")
            $cause=$form['cause']->getData();
            else
            {
                $longitude=$form['longitude']->getData();
                $latitude=$form['latitude']->getData();
             }

            $em = $this->getDoctrine()->getManager();
            $report=$em->getRepository('AppBundle:project')->find ($id);


            $report->setName($name);
            $report->setPriority($priority);
            $report->setDescription($description);
            $report->setType($type);
            if ($type=="general")
            {
                $report->setCause($cause);
                $report->setLongitude(0);
                $report->setLatitude(0);
            }
            else 
            {
                $report->setCause('None');
                $report->setLongitude($longitude);
                $report->setLatitude($latitude);
            }
            $report->setJustification($justification);

            $old = $em ->getRepository('AppBundle:project')->findByName($report->getName());
            foreach ($old as $t)
                $em->remove($t);
            $report->setApproved(1);
            $report->setAccepted(0);
            $em->persist($report);
            $em->flush();
            $this->addFlash('notice', 'Reported Rejected');
            return $this->redirectToRoute('project_list');
        }

        return $this->render('project/rejection.html.twig',array('report'=>$report, 'form'=>$form->createView()));
    }
}

?>